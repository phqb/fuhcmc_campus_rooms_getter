# FPTU HCMC Campus Rooms Getter

## Requirements (no need if using Windows executable)

* Python 3
* pip
* `pip install -r requirements.txt`

## How to use

### Use Windows executable

0. Download [Windows executables](https://bitbucket.org/phqb/fuhcmc_campus_rooms_getter/downloads/getrooms.exe)
1. Login to [fap.fpt.edu.vn](http://fap.fpt.edu.vn)
2. Get `ASP.NET_SessionId` cookie's value using inspection tool
3. Open Command Prompt
4. Paste the folder's location where the executable is stored then press Enter
5. Run `getrooms.exe [Session ID] > rooms.csv` with Session ID as the first argument (without square brackets)
6. Open CSV output file using Excel or something to query 

### Use script

1. Login to [fap.fpt.edu.vn](http://fap.fpt.edu.vn)
2. Get `ASP.NET_SessionId` cookie's value using inspection tool
3. Run `python getrooms.py [Session ID] > output.txt` with Session ID as the first argument (without square brackets)
4. Open CSV output file using Excel or something to query 