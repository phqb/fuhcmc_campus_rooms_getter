#!/usr/bin/python3

import sys
import re
import datetime
import http.client
from bs4 import BeautifulSoup

def soup_fetch(host, path, sessonid):
    print('Fetching ' + host + path, file=sys.stderr)

    conn = http.client.HTTPConnection(host)
    conn.request("GET", path, headers={ 
        "Cookie": "G_ENABLED_IDPS=google; "
                + "ASP.NET_SessionId=" + sessonid + "; "
                + "G_AUTHUSER_H=1" 
    })
    r = conn.getresponse()
    html = r.read().decode('utf-8')
    return BeautifulSoup(html, 'lxml')

def getgroups(host, path, sessionid):
    soup = soup_fetch(host, path, sessionid)
    groupcells = soup.find(id='ctl00_mainContent_divGroup').find_all('td')
    return (cell.find('a').get('href') for cell in groupcells)

SLOT_REGEX_EXTRACTOR = '\d+'

def getrow(r):
    cells = r.find_all('td')[:4]
    date = cells[0].string.split()[1]
    slot = re.search(SLOT_REGEX_EXTRACTOR, cells[1].string).group(0)
    room = cells[2].string
    teacher = cells[3].string
    return ['NA' if cell is None else str(cell) for cell in [date, slot, room, teacher]]

def gettimetable(host, path, room_params, sessionid):
    # Find `group` parameter in a query params, e.x: ?campus=4&term=40&group=ACC101_0
    group = re.search(r"group=(\w+)&?.*$", room_params).groups()[0]
    soup = soup_fetch(host, path + room_params, sessionid)
    rows = soup.find(id='ctl00_mainContent_divDetail').find('table').find('tbody').find_all('tr')
    return ([group] + getrow(r) for r in rows)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please provide fap.fpt.edu.vn Session ID')
    else:
        host = 'fap.fpt.edu.vn'
        path = '/Schedule/TimeTable.aspx'
        sessionid = sys.argv[1]
        groups = getgroups(host, path, sessionid)
        class_timetables = (row for group in groups for row in gettimetable(host, path, group, sessionid))
        csv_rows = [','.join(row) for row in class_timetables]
        print('Group,Day,Slot,Room,Teacher')
        print('\n'.join(csv_rows))